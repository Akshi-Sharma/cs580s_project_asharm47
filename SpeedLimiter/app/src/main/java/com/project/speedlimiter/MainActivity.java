package com.project.speedlimiter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import net.gotev.speech.GoogleVoiceTypingDisabledException;
import net.gotev.speech.Speech;
import net.gotev.speech.SpeechDelegate;
import net.gotev.speech.SpeechRecognitionNotAvailable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 5;
    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 8;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 9;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 2;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 3;
    private static final int REQUEST_MUSIC_PATH = 3;

    Spinner spinner;
    ImageView speed_circle;
    ImageView mic_circle;
    TextView current_speed;
    EditText speed_limit;
    View direction;
    View music;
    View stop_music;

    MediaPlayer beeb_sound;

    Activity activity;

    static final int TIMER_DONE = 2;
    static final int START = 3;
    static final int CAL_TIMER_DONE = 4;
    static final int ERROR = 5;

    private XYZAccelerometer xyzAcc;
    private SensorManager mSensorManager;
    private static final long UPDATE_INTERVAL = 500;
    private static final long MEASURE_TIMES = 10;
    private Timer timer;
    int counter;
    private SpeedData mdXYZ;
    private int small_limit = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;

        initPermissions(); // request permission at runtime
        initElemnt(); // initialize the UI elements in activity_main layout
        initMic(); // initialize on mic icon click
        initLimitEditText(); // initialize on change of limit speed EditText
        initCurrentSpeed(); // initialize speed accelerometer
        initMusicLocationListener(); // initialize music fond location
        initDirectionsListener(); // initialize on roads icon click

    }


    private void initDirectionsListener() {
        direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity, DirectionActivity.class));
            }
        });
    }


    private void initMusicLocationListener() {
        music.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(i, "Choose directory"), REQUEST_MUSIC_PATH); // open select folder for music
            }
        });
    }


    private void initCurrentSpeed() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startCalibrating();
            }
        }, 2000); // start calibration after 2 sec from the app starting

    }

    private void initPermissions() {

        boolean hasPermissionRecordAudio = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionRecordAudio) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
        }

        boolean hasPermissionCallPhone = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionCallPhone) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.CALL_PHONE},
                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
        }

        boolean hasPermissionReadContacts = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionReadContacts) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }
        boolean hasPermissionReadStorage = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionReadStorage) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_STORAGE);
        }
        boolean hasPermissionWriteStorage = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermissionWriteStorage) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    private void initMic() {
        mic_circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptSpeechInput();
            }
        });
    }


    private void promptSpeechInput() {


        try {
            // you must have android.permission.RECORD_AUDIO granted at this point
            Speech.init(getApplicationContext());
            Speech.getInstance().startListening(new SpeechDelegate() {
                @Override
                public void onStartOfSpeech() {
                    mic_circle.setImageDrawable(getResources().getDrawable(R.drawable.circle_record));
                }

                @Override
                public void onSpeechRmsChanged(float value) {
                    Log.d("speech", "rms is now: " + value);

                    // change the circle upon the power of the voice (rms)
                    if (Math.abs(value) < 3) {
                        mic_circle.animate().scaleX(1f).start();
                        mic_circle.animate().scaleY(1f).start();
                    } else {
                        mic_circle.animate().scaleX((value / 10) + 1).start();
                        mic_circle.animate().scaleY((value / 10) + 1).start();
                    }
                }

                @Override
                public void onSpeechPartialResults(List<String> results) {
                    StringBuilder str = new StringBuilder();
                    for (String res : results) {
                        str.append(res).append(" ");
                    }

                    Log.i("speech", "partial result: " + str.toString().trim());
                }

                @Override
                public void onSpeechResult(String result) {


                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show(); // show result of speech to user

                    // scale to circle to original
                    mic_circle.animate().scaleX(1).start();
                    mic_circle.animate().scaleY(1).start();
                    mic_circle.setImageDrawable(getResources().getDrawable(R.drawable.cirlce_small));

                    if (!result.isEmpty()) {

                        result = result.toLowerCase();

                        if (result.contains("call") || result.contains("dial")) {

                            result = result.replace("call ", "");
                            result = result.replace("dial ", "");

                            String number = "";

                            if (result.matches(".*\\d.*")) {
                                number = result.replaceAll("\\D+", "");
                            } else {
                                number = getContactList(result);
                            }




                            if (!Objects.equals(number, "")) {
                                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));

                                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                                    startActivity(callIntent);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Invalid command", Toast.LENGTH_SHORT).show();
                            }


                        } else if (result.contains("play")) {
                            String path = getDefaults("music", getApplicationContext());



                            result = result.replace("play ", "");
                            result = result.replace(" Song", "");

                            File yourDir = new File(path);

                            OUTERMOST:
                            for (File f : yourDir.listFiles()) {
                                if (f.isFile())
                                    if (f.getName().endsWith(".mp3") || f.getName().endsWith(".MP3")) {


                                        if (f.getName().toLowerCase().contains(result)) {

                                            final MediaPlayer song = MediaPlayer.create(getApplicationContext(), Uri.parse(path + "/" + f.getName()));
                                            song.start();

                                            stop_music.setVisibility(View.VISIBLE);
                                            stop_music.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    song.stop();
                                                    stop_music.setVisibility(View.GONE);
                                                }
                                            });
                                            song.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                @Override
                                                public void onCompletion(MediaPlayer mediaPlayer) {
                                                    mediaPlayer.stop();
                                                    stop_music.setVisibility(View.GONE);
                                                }
                                            });
                                            break OUTERMOST;
                                        }
                                    }


                            }

                            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                        }
                    } else {

                    }
                }
            });
        } catch (SpeechRecognitionNotAvailable exc) {

            Toast.makeText(getApplicationContext(), "Speech recognition is not available on this device!", Toast.LENGTH_SHORT).show();

        } catch (GoogleVoiceTypingDisabledException exc) {

        }

    }


    private void initLimitEditText() {
        speed_limit.setText(getDefaultsSpeed("speed", getApplicationContext()));
        speed_limit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                setDefaults("speed", editable.toString(), getApplicationContext());
            }
        });

    }


    private void initElemnt() {
        direction = findViewById(R.id.direction);
        music = findViewById(R.id.music);
        current_speed = findViewById(R.id.current_speed);
        spinner = findViewById(R.id.spinner);
        speed_circle = findViewById(R.id.speed_circle);
        mic_circle = findViewById(R.id.mic_circle);
        speed_limit = findViewById(R.id.speed_limit);
        stop_music = findViewById(R.id.stop_music);

        beeb_sound = MediaPlayer.create(this, R.raw.beep);
    }



    @SuppressLint("HandlerLeak")
    Handler hRefresh = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case TIMER_DONE:


                    onMeasureDone();

                    int sp = (int) (mdXYZ.getLastSpeedKm() - 17);
                    if (sp < 0 || sp > 200) { // speed less than zero or greater than 200 km put speed equal zero
                        sp = 0;
                    }

                    // update UI
                    current_speed.setText(String.valueOf(sp));
                    speed_circle.setImageDrawable(getResources().getDrawable(R.drawable.circle));

                    if (getDefaults("speed", getApplicationContext()).length() != 0) {
                        if (Integer.valueOf(getDefaults("speed", getApplicationContext())) < sp) {
                            beeb_sound.start();
                            speed_circle.setImageDrawable(getResources().getDrawable(R.drawable.circle_red));
                        }

                    }
                    if (small_limit != 0) {
                        if (small_limit < sp) { // speed gets over the limit
                            beeb_sound.start();
                            speed_circle.setImageDrawable(getResources().getDrawable(R.drawable.circle_red));

                        }

                    }

                    startCalibrating();

                    break;
                case START:


                    timer = new Timer();
                    timer.scheduleAtFixedRate(
                            new TimerTask() {

                                public void run() {
                                    dumpSensor();
                                }
                            },
                            0,
                            UPDATE_INTERVAL);

                    break;
                case ERROR:
                    Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
                default:

                    break;
            }
        }
    };

    void dumpSensor() {
        ++counter;
        mdXYZ.addPoint(xyzAcc.getPoint());

        hRefresh.sendEmptyMessage(7);

        if (counter > MEASURE_TIMES) {
            timer.cancel();
            hRefresh.sendEmptyMessage(TIMER_DONE);
        }

    }

    public void startCalibrating() {

        mdXYZ = new SpeedData(UPDATE_INTERVAL);
        counter = 0;

        Calibrator cal = new Calibrator(hRefresh, xyzAcc, START);
        cal.calibrate();

    }


    private void setAccelerometer() {
        xyzAcc = new XYZAccelerometer();
        mSensorManager.registerListener(xyzAcc,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);
    }


    private void onMeasureDone() {
        try {
            mdXYZ.process();
            long now = System.currentTimeMillis();
            mdXYZ.saveExt(this, Long.toString(now) + ".csv");
        } catch (Throwable ex) {

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        setAccelerometer();

        mSensorManager.registerListener(xyzAcc,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);

        initSpinnerItems();


    }

    private void initSpinnerItems() {
        final List<String> list = new ArrayList<String>();


        String sfrom_1 = getDefaults("from_1", getApplicationContext());
        String sfrom_2 = getDefaults("from_2", getApplicationContext());
        String sfrom_3 = getDefaults("from_3", getApplicationContext());
        String sfrom_4 = getDefaults("from_4", getApplicationContext());
        String sfrom_5 = getDefaults("from_5", getApplicationContext());

        String sto_1 = getDefaults("to_1", getApplicationContext());
        String sto_2 = getDefaults("to_2", getApplicationContext());
        String sto_3 = getDefaults("to_3", getApplicationContext());
        String sto_4 = getDefaults("to_4", getApplicationContext());
        String sto_5 = getDefaults("to_5    ", getApplicationContext());

        final String speed_1 = getDefaults("speed_1", getApplicationContext());
        final String speed_2 = getDefaults("speed_2", getApplicationContext());
        final String speed_3 = getDefaults("speed_3", getApplicationContext());
        final String speed_4 = getDefaults("speed_4", getApplicationContext());
        final String speed_5 = getDefaults("speed_5", getApplicationContext());


        final String road_1 = sfrom_1 + " - " + sto_1;
        final String road_2 = sfrom_2 + " - " + sto_2;
        final String road_3 = sfrom_3 + " - " + sto_3;
        final String road_4 = sfrom_4 + " - " + sto_4;
        final String road_5 = sfrom_5 + " - " + sto_5;


        if (road_1.length() > 3 && speed_1.length() > 0) {
            list.add(road_1 + " " + speed_1);
        }
        if (road_2.length() > 3 && speed_2.length() > 0) {
            list.add(road_2 + " " + speed_2);
        }
        if (road_3.length() > 3 && speed_3.length() > 0) {
            list.add(road_3 + " " + speed_3);
        }
        if (road_4.length() > 3 && speed_4.length() > 0) {
            list.add(road_4 + " " + speed_4);
        }
        if (road_5.length() > 3 && speed_5.length() > 0) {
            list.add(road_5 + " " + speed_5);
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                if (Objects.equals(list.get(i), road_1 + " " + speed_1)) {
                    small_limit = Integer.valueOf(speed_1);
                } else if (Objects.equals(list.get(i), road_2 + " " + speed_2)) {
                    small_limit = Integer.valueOf(speed_2);
                } else if (Objects.equals(list.get(i), road_3 + " " + speed_3)) {
                    small_limit = Integer.valueOf(speed_3);
                } else if (Objects.equals(list.get(i), road_4 + " " + speed_4)) {
                    small_limit = Integer.valueOf(speed_4);
                } else if (Objects.equals(list.get(i), road_5 + " " + speed_5)) {
                    small_limit = Integer.valueOf(speed_5);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        if (spinner.getSelectedItem() != null) {
            String current_small_string = spinner.getSelectedItem().toString();

            if (Objects.equals(current_small_string, road_1 + " " + speed_1)) {
                small_limit = Integer.valueOf(speed_1);
            } else if (Objects.equals(current_small_string, road_2 + " " + speed_2)) {
                small_limit = Integer.valueOf(speed_2);
            } else if (Objects.equals(current_small_string, road_3 + " " + speed_3)) {
                small_limit = Integer.valueOf(speed_3);
            } else if (Objects.equals(current_small_string, road_4 + " " + speed_4)) {
                small_limit = Integer.valueOf(speed_4);
            } else if (Objects.equals(current_small_string, road_5 + " " + speed_5)) {
                small_limit = Integer.valueOf(speed_5);
            }
        }

    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(xyzAcc);
        super.onPause();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initPermissions();

                } else {
//
                }

                return;

            }
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initPermissions();

                } else {

                }
                return;

            }
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initPermissions();

                } else {

                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initPermissions();

                } else {

                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_WRITE_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initPermissions();

                } else {

                }
                return;
            }


            default: {

            }
        }
    }


    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "");
    }

    public static String getDefaultsSpeed(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "100");
    }


    private String getContactList(String contact_name) {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);



        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));


                        if (Objects.equals(name.toLowerCase(), contact_name.toLowerCase())) {



                            return phoneNo;
                        }
                    }
                    pCur.close();
                }
            }
        }
        if (cur != null) {
            cur.close();
        }

        return "";
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_MUSIC_PATH:

                    Uri treeUri = data.getData();
                    String path = FileUtil.getFullPathFromTreeUri(treeUri, getApplicationContext());



                    setDefaults("music", path, getApplicationContext());

                    break;
            }

        }

    }


}