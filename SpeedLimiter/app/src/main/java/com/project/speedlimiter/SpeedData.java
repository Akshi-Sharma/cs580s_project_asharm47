package com.project.speedlimiter;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class SpeedData {

    private ArrayList<Point> accData;
    private ArrayList<SpeedPoint> data;

    private long interval;

    public SpeedData(long interval) {
        this.interval = interval;
        accData = new ArrayList<Point> ();
        data = new ArrayList<SpeedPoint> ();
    }

    public void addPoint(Point p){
        accData.add(p);
    }

    public void process(){

        for(int i = 0; i < accData.size(); ++i){
            Point p = (Point) accData.get(i);
            float speed = 0;

            if(i > 0){
                speed = data.get(i-1).speedAfter;
            }
            data.add(new SpeedPoint(p.getX(), p.getY(), p.getZ(), speed, interval, getAveragePoint()));
        }
    }

    public boolean saveExt(Context con, String fname) throws Throwable {

        try {

            File file = new File(con.getExternalFilesDir(null), fname);
            FileOutputStream os = new FileOutputStream(file);
            OutputStreamWriter out = new OutputStreamWriter(os);


            for (int i = 0; i < data.size(); ++i) {
                SpeedPoint m = (SpeedPoint) data.get(i);
                out.write(m.getStoreString());
            }

            out.close();
        } catch (Throwable t) {
            throw (t);
        }
        return true;
    }

    private Point getAveragePoint() {
        float x = 0;
        float y = 0;
        float z = 0;

        for(int i = 0; i < accData.size(); ++i){
            Point p = (Point) accData.get(i);
            x += p.getX();
            y += p.getY();
            z += p.getZ();
        }

        return new Point(x, y, z, 1);
    }

    public float getLastSpeed()
    {
        return data.get(data.size() - 1).speedAfter;
    }


    public float getLastSpeedKm(){
        float ms = getLastSpeed();
        return ms*3.6f;
    }
}
