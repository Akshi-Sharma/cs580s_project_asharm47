package com.project.speedlimiter;

import android.os.Handler;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Calibrator {

    final static int UPDATE_INTERVAL = 400;
    final static int ITERATIONS = 5;
    int seqcount;
    private ArrayList<Point> calData;
    Handler hRefresh;
    XYZAccelerometer acclrxyz;


    public Calibrator(Handler hRefresh, XYZAccelerometer acclrxyz, int seqcount)
    {
        this.acclrxyz = acclrxyz;
        this.seqcount = seqcount;
        this.hRefresh = hRefresh;

    }

    public void calibrate() {
        final Timer calTimer = new Timer();
        calData = new ArrayList<>();
        acclrxyz.setdX(0);
        acclrxyz.setdY(0);
        acclrxyz.setdZ(0);

        calTimer.scheduleAtFixedRate(
                new TimerTask()
                {
                    public void run()
                    {
                        addCalData(calData);
                        if (calData.size() > ITERATIONS)
                        {
                            calTimer.cancel();
                            try
                            {
                                calSensor(calData);
                            } catch (Exception ex)
                            {
                                try
                                {
                                    throw ex;
                                } catch (Exception ex1)
                                {
                                    hRefresh.sendEmptyMessage(5);
                                }
                            }
                            hRefresh.sendEmptyMessage(seqcount);
                        }
                    }
                },
                0,
                UPDATE_INTERVAL);
    }

    private void addCalData(ArrayList<Point> cD) {
        Point p = acclrxyz.getPoint();
        cD.add(p);
        acclrxyz.reset();
    }

    private void calSensor(ArrayList<Point> cD) throws Exception {
        if (cD.size() < ITERATIONS-1)
        {
            throw new Exception("caliberation not possible insufficient data");
        }
        float x = 0;
        float y = 0;
        float z = 0;

        for (int i = 1; i < cD.size(); ++i) {
            x += cD.get(i).getX();
            y += cD.get(i).getY();
            z += cD.get(i).getZ();
        }

        x = x / (cD.size() - 1);
        y = y / (cD.size() - 1);
        z = z / (cD.size() - 1);

        acclrxyz.setdX(-x);
        acclrxyz.setdY(-y);
        acclrxyz.setdZ(-z);
    }
}
