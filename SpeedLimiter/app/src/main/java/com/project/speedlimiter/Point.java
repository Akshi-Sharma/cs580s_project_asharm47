package com.project.speedlimiter;

public class Point {
    private float x = 0;
    private float y = 0;
    private float z = 0;
    private int cnt = 1;
    float a;

    public float getX() {
        a = x/(float)cnt;
        return a;
    }

    public float getY() {

        return y/(float)cnt;
    }

    public float getZ() {

        return z/(float)cnt;
    }

    public Point(float m, float n, float o, int cnt) {
        this.x = m;
        this.y = n;
        this.cnt = cnt;
        this.z = o;

    }


    public float getForce(){

        float x1 = getX()*getX();
        float y1 = getY()*getY();
        float z1 = getZ()*getZ();

        return x1 + y1 + z1;
    }
}