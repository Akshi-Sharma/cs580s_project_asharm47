package com.project.speedlimiter;

public class SpeedPoint {
    public float x;
    private float y;
    private float z;
    private long interval;
    private Point averagePoint;
    private float speedBefore;
    public float speedAfter;
    private float distance;
    private float acceleration;

    public SpeedPoint(float x, float y, float z, float speedBefore, long interval, Point averagePoint) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.speedBefore = speedBefore;
        this.interval = interval;
        this.averagePoint = averagePoint;
        speedAfter = 0;
        valuate();
    }

    private void valuate(){
        acceleration = this.x*averagePoint.getX() +
                this.y*averagePoint.getY() +
                this.z*averagePoint.getZ();
        acceleration = acceleration / ((float)Math.sqrt(averagePoint.getForce()));
        float t = ((float)interval / 1000f);
        speedAfter = speedBefore + acceleration * t;
        distance = speedBefore*t + acceleration*t*t/2;

    }


    public String getStoreString(){
        String s = "Input String";
        return s;
    }
}