package com.project.speedlimiter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import static com.project.speedlimiter.MainActivity.getDefaults;

public class DirectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direction);


        final EditText from_1 = (EditText) findViewById(R.id.from_1);
        final EditText from_2 = (EditText) findViewById(R.id.from_2);
        final EditText from_3 = (EditText) findViewById(R.id.from_3);
        final EditText from_4 = (EditText) findViewById(R.id.from_4);
        final EditText from_5 = (EditText) findViewById(R.id.from_5);


        final EditText to_1 = (EditText) findViewById(R.id.to_1);
        final EditText to_2 = (EditText) findViewById(R.id.to_2);
        final EditText to_3 = (EditText) findViewById(R.id.to_3);
        final EditText to_4 = (EditText) findViewById(R.id.to_4);
        final EditText to_5 = (EditText) findViewById(R.id.to_5);


        final EditText speed_1 = (EditText) findViewById(R.id.speed_1);
        final EditText speed_2 = (EditText) findViewById(R.id.speed_2);
        final EditText speed_3 = (EditText) findViewById(R.id.speed_3);
        final EditText speed_4 = (EditText) findViewById(R.id.speed_4);
        final EditText speed_5 = (EditText) findViewById(R.id.speed_5);


        View view = findViewById(R.id.dir_done);



        final String sfrom_1 = getDefaults("from_1", getApplicationContext());
        final String sfrom_2 = getDefaults("from_2", getApplicationContext());
        final String sfrom_3 = getDefaults("from_3", getApplicationContext());
        final String sfrom_4 = getDefaults("from_4", getApplicationContext());
        final String sfrom_5 = getDefaults("from_5", getApplicationContext());


        final String sto_1 = getDefaults("to_1", getApplicationContext());
        final String sto_2 = getDefaults("to_2", getApplicationContext());
        final String sto_3 = getDefaults("to_3", getApplicationContext());
        final String sto_4 = getDefaults("to_4", getApplicationContext());
        final String sto_5 = getDefaults("to_5", getApplicationContext());


        final String sspeed_1 = getDefaults("speed_1", getApplicationContext());
        final String sspeed_2 = getDefaults("speed_2", getApplicationContext());
        final String sspeed_3 = getDefaults("speed_3", getApplicationContext());
        final String sspeed_4 = getDefaults("speed_4", getApplicationContext());
        final String sspeed_5 = getDefaults("speed_5", getApplicationContext());



        if (sfrom_1.length() != 0)
            from_1.setText(sfrom_1);
        if (sfrom_2.length() != 0)
            from_2.setText(sfrom_2);
        if (sfrom_3.length() != 0)
            from_3.setText(sfrom_3);
        if (sfrom_4.length() != 0)
            from_4.setText(sfrom_4);
        if (sfrom_5.length() != 0)
            from_5.setText(sfrom_5);

        if (sto_1.length() != 0)
            to_1.setText(sto_1);
        if (sto_2.length() != 0)
            to_2.setText(sto_2);
        if (sto_3.length() != 0)
            to_3.setText(sto_3);
        if (sto_4.length() != 0)
            to_4.setText(sto_4);
        if (sto_5.length() != 0)
            to_5.setText(sto_5);

        if (sspeed_1.length() != 0)
            speed_1.setText(sspeed_1);
        if (sspeed_2.length() != 0)
            speed_2.setText(sspeed_2);
        if (sspeed_3.length() != 0)
            speed_3.setText(sspeed_3);
        if (sspeed_4.length() != 0)
            speed_4.setText(sspeed_4);
        if (sspeed_5.length() != 0)
            speed_5.setText(sspeed_5);




        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String sfrom_1 = from_1.getText().toString();
                String sfrom_2 = from_2.getText().toString();
                String sfrom_3 = from_3.getText().toString();
                String sfrom_4 = from_4.getText().toString();
                String sfrom_5 = from_5.getText().toString();

                String sto_1 = to_1.getText().toString();
                String sto_2 = to_2.getText().toString();
                String sto_3 = to_3.getText().toString();
                String sto_4 = to_4.getText().toString();
                String sto_5 = to_5.getText().toString();

                MainActivity.setDefaults("from_1", sfrom_1, getApplicationContext());
                MainActivity.setDefaults("from_2", sfrom_2, getApplicationContext());
                MainActivity.setDefaults("from_3", sfrom_3, getApplicationContext());
                MainActivity.setDefaults("from_4", sfrom_4, getApplicationContext());
                MainActivity.setDefaults("from_5", sfrom_5, getApplicationContext());

                MainActivity.setDefaults("to_1", sto_1, getApplicationContext());
                MainActivity.setDefaults("to_2", sto_2, getApplicationContext());
                MainActivity.setDefaults("to_3", sto_3, getApplicationContext());
                MainActivity.setDefaults("to_4", sto_4, getApplicationContext());
                MainActivity.setDefaults("to_5", sto_5, getApplicationContext());

                MainActivity.setDefaults("speed_1", speed_1.getText().toString(), getApplicationContext());
                MainActivity.setDefaults("speed_2", speed_2.getText().toString(), getApplicationContext());
                MainActivity.setDefaults("speed_3", speed_3.getText().toString(), getApplicationContext());
                MainActivity.setDefaults("speed_4", speed_4.getText().toString(), getApplicationContext());
                MainActivity.setDefaults("speed_5", speed_5.getText().toString(), getApplicationContext());

                finish();

            }
        });



    }







}











